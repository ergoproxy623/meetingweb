export interface CategoryValuesDto {
  id: number;
  value: number;
  title: string;
  icon: string;
}
